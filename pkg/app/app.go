package app

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type App struct {
	router *gin.Engine
	config *Config
}

func NewApp(conf *Config) *App {
	return &App{
		router: gin.Default(),
		config: conf,
	}
}

func (app *App) Run() {
	app.router.GET("/healthz", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "OK",
			"message": "Service a is running well",
		})
	})
	app.router.GET("/api/v1/features", func(c *gin.Context) {
		c.JSON(http.StatusOK, &app.config)
	})
	app.router.Run(fmt.Sprintf(":%s", app.config.Port))
}
