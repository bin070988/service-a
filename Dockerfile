FROM golang:1.16.12-stretch as build
ENV GOPROXY "https://goproxy.cn,direct"
ENV CGO_ENABLED 0
RUN mkdir /build
COPY . /build/
WORKDIR /build
RUN ls -l
RUN go build -o service-a ./main.go

FROM alpine:3.15
RUN apk add tzdata && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
COPY --from=build /build/service-a /
COPY --from=build /build/config.yaml /
ENTRYPOINT [ "/service-a" ]
EXPOSE 8888
